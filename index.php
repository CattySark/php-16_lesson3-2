<?php
 /*Полиморфизм это возможность изменять какие-то свойства объектов или методов локально. Т.е. если у нас есть,
 //к примеру, какой-то класс с некоторыми своствами А, Б и методами С, Д, то мы можем сказать, что в наследнике этого
 класса или в конкретном объекте свойство А будет значить П, а метод Д выполнять функцию Ж.
-- Образно: абстрактный класс - это как родитель,который не может выходит на улицу. У него есть всё, 
 что может быть у класса, кроме создания объектов с помощью  new. А интерфейс - это скорее одежда,
 которую должен одеть класс, прежде чем его выпустят на улицу. У него нет свойств, но во все методы нужно вставить 
 реализацию конкретного класса(конкретные руки, ноги и пр.). Я думаю, что изначально при написании кода логично использовать 
абстрактные классы, а потом, при нехватке функционала класса, добавлять интерфейсы.*/
// если я правильно поняла, то суперкласс может быть абстрактным классом.

abstract class Real //оксюморон какой-то)))
{
  abstract function getColor();
}
interface Model
{
  public function takeModel($model);
}
class Car extends Real implements Model
 {
     public $color='black';
     public $power=100;
     public $model;

     public function takeModel($car)
     {
         if($model='BMV') $power=200;
         elso ($color='red');
     }

    function getColor() {
         return $this->color;
     }
 }

  $car=new Car;
  $car->model='BMV';
  takeModel($car);
  echo $car->power;

class TVr extends Real implements Model
{
    public $price;

    public function takeModel($TV){
        if($model='LG') $price=200;
        elso (echo 'сколько дашь?');
    }
    function getColor() {
        return 'color';
    }

}
$TV1=new TVr;
$TV1->brand='LG';
$TV2=new TVr;
$TV2->brand='Samsung';
takeModel ($TV1);

class Ballpoint extends Real
{
    public $color='blue';
    public $use=0;

    public function beUse(){
        $use=1;
    }
    function getColor() {
        return $this->color;
    }
}

$pen=new Ballpoint;
$pensil=new Ballpoint;

class Duck extends Real
{
    public $country;

  public function getCountry(){
      return $this->country;
  }
    function getColor() {
        return 'notcolor';
    }
}
$DonaldDuck=new Duck;
$DonaldDuck->country='Америка';
$uglyDuck=new Duck;
$uglyDuck->country='Россия';

class Product extends Real
{
    public $price;
    public $name;
    public $color;

    function getColor()
    {
        return $this->color;
    }

    public function __construct($name, $price)
    {
        $this->name = $name;
        $this->price = $price;

    }
}
$table = new Product('стол', 10000);
$chair = new Product('стул', 500);

  echo $table->name;
  echo $chair->name;



?>